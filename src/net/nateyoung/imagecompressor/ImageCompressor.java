package net.nateyoung.imagecompressor;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Iterator;
import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.stream.ImageOutputStream;

/**
 *
 * @author Nate Young
 * 
 */
public class ImageCompressor {

    /**
     * @param args the command line arguments
     * args[0] is the image
     * args[1] is the compression quality and is an option parameter
     */
    public static void main(String[] args){
        
        System.out.println("Starting image compressor");
        
        if (args.length < 1) {
            printHelp();
        }
        
        File imageFile = new File(args[0]); //The image file to compress
        
        float compressionQuality = .3f;
        
        if (args.length >= 2) {
           compressionQuality = Float.parseFloat(args[1]);
        }
            
        if (isJPEGFile(imageFile)) {
            try {    
                File compressedImage = proccessImage(imageFile, compressionQuality);
                
                System.out.println("The compressed image has been generated: " + compressedImage.getName());
            } catch (FileNotFoundException e) {
                System.out.println("The supplied file could not be found, please make sure you spelled it's name correctly.");
            } catch (IOException e) {
                System.out.println("Input/Output error generated. The streams couldn't be properly opened or closed. Please make sure this program is run with the proper permissions");
            } catch (IllegalStateException e) {
                System.out.println("Image Writer Error");
            }
        } else {
            System.out.println("The supplied image was not a jpeg");
        }
    }
    
    /**
     * 
     * @param imageFile to compress
     * @param imageQuality
     * @return the compressed image
     * @throws java.io.FileNotFoundException
     * @throws IOException
     * @throws IllegalStateException
     */
    public static File proccessImage(File imageFile, float imageQuality) throws FileNotFoundException, IOException, IllegalStateException {

        File compressedImage = new File("compressed_" + imageFile.getName());
        
       InputStream inputStream;
       inputStream = new FileInputStream(imageFile);
       
       OutputStream outputStream;
       outputStream = new FileOutputStream(compressedImage);
        
       BufferedImage bufferedImage = ImageIO.read(inputStream);
       
       Iterator<ImageWriter> imageWriters = ImageIO.getImageWritersByFormatName("jpg");
       
       if (!imageWriters.hasNext()) {
           throw new IllegalStateException("Image Writer Error");           
       }
       
       ImageWriter imageWriter = (ImageWriter) imageWriters.next();
       ImageOutputStream imageOutputStream;
       imageOutputStream = ImageIO.createImageOutputStream(outputStream);
       imageWriter.setOutput(imageOutputStream);
 
       ImageWriteParam imageWriteParam = imageWriter.getDefaultWriteParam();

       imageWriteParam.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
       imageWriteParam.setCompressionQuality(imageQuality);
       
       imageWriter.write(null, new IIOImage(bufferedImage, null, null), imageWriteParam);

       //Cleanup all streams and writers
       outputStream.close();
       inputStream.close();
       imageOutputStream.close();
       imageWriter.dispose();
       
       return compressedImage;
       
    }
    
    /**
     * 
     * @param imageFile to verify
     * @return boolean of whether or not the given file was a jpeg
     */
    public static boolean isJPEGFile(File imageFile) {
        boolean isJPEG = false;
        
        String fileName = imageFile.getName();
        
        String[] splitFileName = fileName.split(".");
        
        if (splitFileName.length > 1) {
            System.out.println(splitFileName[1]);
            if (splitFileName[1].equalsIgnoreCase(".jpg") || splitFileName[1].equalsIgnoreCase(".jpeg")) {
                isJPEG = true;
            } else {
                isJPEG = false;
            }
            
        } else {
            isJPEG = false;
        }
        
        return true;
    }
    
    public static void printHelp() {
        System.out.println("java ImageCompressor.jar <file> <quality>");
    }
    
}
